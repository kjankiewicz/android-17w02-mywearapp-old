package com.example.kjankiewicz.mywearapp;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class MyMainActivity extends ActionBarActivity {

    int selectedItem;

    ArrayList<String> mListToDo;
    StringArrayAdapter mAdapterToDo;

    private class StringArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StringArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_main);

        final ListView listViewToDo = (ListView) findViewById(R.id.listViewToDo);

        mListToDo = new ArrayList<>();

        mListToDo.addAll(ToDoListMaster.getToDoList(getApplicationContext()));

        mAdapterToDo = new StringArrayAdapter(this,
                android.R.layout.simple_list_item_1, mListToDo);
        listViewToDo.setAdapter(mAdapterToDo);
        ToDoListMaster.saveToDoList(getApplicationContext(), mListToDo);

        listViewToDo.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listViewToDo.setMultiChoiceModeListener(new MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                // Here you can do something when items are selected/de-selected,
                // such as update the title in the CAB
                Log.d("tag", "position: " + position);
                selectedItem = position;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Respond to clicks on the actions in the CAB
                switch (item.getItemId()) {
                    case R.id.menu_add :
                        addDialog().show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    case R.id.menu_delete :
                        deleteDialog().show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    case R.id.menu_edit :
                        editDialog().show();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default :
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate the menu for the CAB
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context_menu, menu);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // Here you can make any necessary updates to the activity when
                // the CAB is removed. By default, selected items are deselected/unchecked.

            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // Here you can perform updates to the CAB due to
                // an invalidate() request
                return false;
            }
        });
    }

    @Override
    protected void onPause() {
        ToDoListMaster.saveToDoList(getApplicationContext(), mListToDo);
        super.onPause();
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.context_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    public Dialog addDialog() {
        Builder builder = new Builder(this);
        final View layout = View.inflate(this, R.layout.edit_dialog, null);

        final EditText editToDoName = (EditText) layout.findViewById(R.id.edit_todo_name);

        builder.setTitle(this.getResources().getString(R.string.dialog_add));
        builder.setView(layout);
        builder.setPositiveButton(getResources().getString(R.string.ok), new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                if (editToDoName.getText().length()>0) {
                    mListToDo.add(editToDoName.getText().toString());
                    dialog.dismiss();

                    mAdapterToDo.notifyDataSetChanged();
                }
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();

    }

    public Dialog editDialog() {
        Builder builder = new Builder(this);
        final View layout = View.inflate(this, R.layout.edit_dialog, null);

        final EditText editToDoName = (EditText) layout.findViewById(R.id.edit_todo_name);
        editToDoName.setText(mListToDo.get(selectedItem));

        builder.setTitle(this.getResources().getString(R.string.dialog_edit));
        builder.setView(layout);
        builder.setPositiveButton(getResources().getString(R.string.ok), new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                mListToDo.set(selectedItem, editToDoName.getText().toString());
                dialog.dismiss();

                mAdapterToDo.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();

    }

    public Dialog deleteDialog() {
        Builder builder = new Builder(this);

        builder.setTitle(this.getResources().getString(R.string.dialog_delete));
        builder.setMessage(this.getResources().getString(R.string.delete_prompt) + " " +
                mListToDo.get(selectedItem) + " todo item?");
        builder.setPositiveButton(getResources().getString(R.string.ok), new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                mListToDo.remove(selectedItem);
                dialog.dismiss();

                mAdapterToDo.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();

    }
}
