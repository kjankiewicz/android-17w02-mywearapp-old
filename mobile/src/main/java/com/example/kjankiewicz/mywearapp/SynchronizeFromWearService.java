package com.example.kjankiewicz.mywearapp;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by kjankiewicz on 2015-05-31.
 */
public class SynchronizeFromWearService extends WearableListenerService {

    private static final String START_ACTIVITY_PATH = "/start-activity";

    private static final String TAG = "SynchroFromWearService";

    GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

        Log.d(TAG, "onDataChanged: " + dataEvents);

        final ArrayList<DataEvent> events = FreezableUtils
                .freezeIterable(dataEvents);

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        ConnectionResult connectionResult =
                googleApiClient.blockingConnect(30, TimeUnit.SECONDS);

        if (!connectionResult.isSuccess()) {
            Log.e(TAG, "Failed to connect to GoogleApiClient.");
            return;
        }

        for (DataEvent event : dataEvents) {
            Log.d(TAG, "getType: " + event.getType() +
                    " TYPE_CHANGED:" + DataEvent.TYPE_CHANGED +
                    " TYPE_DELETED:" + DataEvent.TYPE_DELETED + " " + dataEvents);
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                DataMapItem dataMapItem =
                        DataMapItem.fromDataItem(event.getDataItem());
                DataMap dataMap = dataMapItem.getDataMap();
                String toDoListAsString = dataMap.getString("/to_do_list");
                ToDoListMaster.saveToDoList(getApplicationContext(),
                        ToDoListMaster.getToDoListFromString(toDoListAsString) );
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                ToDoListMaster.saveToDoList(getApplicationContext(),
                        ToDoListMaster.getToDoListFromString("") );
            }

        }
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        if (messageEvent.getPath().equals(START_ACTIVITY_PATH)) {

            Intent startIntent = new Intent(this, MyMainActivity.class);
            startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startIntent);
        }
    }
}
