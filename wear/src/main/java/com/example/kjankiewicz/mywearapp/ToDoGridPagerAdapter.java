package com.example.kjankiewicz.mywearapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.view.Gravity;

import java.util.List;

public class ToDoGridPagerAdapter extends FragmentGridPagerAdapter {

    //private static final float MAXIMUM_CARD_EXPANSION_FACTOR = 3.0f;

    private Context mContext;
    private List<ToDoPickerActivity.GroupToDoList> mData;

    public ToDoGridPagerAdapter(Context context, List<ToDoPickerActivity.GroupToDoList> groupToDoLists, FragmentManager fm) {
        super(fm);
        mContext = context;
        mData = groupToDoLists;
    }

    @Override
    public Fragment getFragment(int row, int column) {
        ToDoPickerActivity.GroupToDoList toDoList = mData.get(row);
        CardFragment fragment = CardFragment.create(toDoList.getGroupName(column), toDoList.getText(column));
        fragment.setCardGravity(Gravity.BOTTOM);
        fragment.setExpansionEnabled(true);
        fragment.setExpansionDirection(CardFragment.EXPAND_DOWN);
        return fragment;
    }

    @Override
    public int getRowCount() {
        return mData.size();
    }

    @Override
    public int getColumnCount(int row) {
        return mData.get(row).getPageCount();
    }


}
