package com.example.kjankiewicz.mywearapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by kjankiewicz on 2015-05-31.
 */
public class ToDoListMaster {

    public static String PREFS_TODO_LIST_NAME = "TODO_LIST_APP";
    public static String TODO_LIST_NAME = "ToDo_List";


    public static ArrayList<String> getToDoList(Context context) {
        ArrayList<String> listToDo = new ArrayList<>();
        SharedPreferences settings;
        settings = context.getSharedPreferences(PREFS_TODO_LIST_NAME,
                Context.MODE_PRIVATE);
        String serializedToDoList;
        serializedToDoList = settings.getString(TODO_LIST_NAME, "");

        listToDo.addAll(Arrays.asList(TextUtils.split(serializedToDoList, ";")));

        if (listToDo.size()==0) {
            String[] defaultToDoList = new String[] {"do shopping",
                    "go to the cinema", "do homework", "walk the dog", "do exercises"};
            Collections.addAll(listToDo, defaultToDoList);
        }
        return listToDo;
    }

    public static void saveToDoList(Context context, ArrayList<String> todoList) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_TODO_LIST_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putString(TODO_LIST_NAME, TextUtils.join(";", todoList));
        editor.apply();
    }

}
