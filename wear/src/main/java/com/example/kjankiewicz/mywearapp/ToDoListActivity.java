package com.example.kjankiewicz.mywearapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.Collections;

public class ToDoListActivity extends Activity
        implements WearableListView.ClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static String TAG = "ToDoListActivity";

    private static final String START_ACTIVITY_PATH = "/start-activity";

    private WearableListView mListView;
    private String[] mStringListToDo = {"ToDo List","Create ToDo","Info Default","Info with Confirmation", "Synchronize with Mobile", "Run on Mobile"};
    ArrayList<String> mListToDo;

    GoogleApiClient mGoogleApiClient;
    Node mNode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);
        mListView = (WearableListView) findViewById(R.id.wearable_todo_list);
        mListToDo = new ArrayList<String>();
        Collections.addAll(mListToDo, mStringListToDo);
        mListView.setAdapter(new ToDoListAdapter(mListToDo));

        mListView.setClickListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        Integer i = viewHolder.getPosition();
        Intent intent;
        switch (i) {
            case 0: //"List" - 2D Picker
                intent = new Intent(this, ToDoPickerActivity.class);
                startActivity(intent);
                break;
            case 1: //"Create" - Voice Input
                intent = new Intent(this, ToDoRecordNewActivity.class);
                startActivity(intent);
                break;
            case 2:  //Info - Default Card
                intent = new Intent(this, ToDoInfoDefaultActivity.class);
                startActivity(intent);
                break;
            case 3: //Info - Card with Confirmation
                intent = new Intent(this, ToDoConfirmationActivity.class);
                startActivity(intent);
                break;
            case 4:
                synchronizeData();
                break;
            case 5:
                sendMessage();
                break;
        }
    }

    @Override
    public void onTopEmptyRegionClick() {

    }



    private void synchronizeData() {

        PutDataMapRequest dataMapRequest = PutDataMapRequest.create("/to_do_path");

        DataMap dataMap = dataMapRequest.getDataMap();

        dataMap.putString("/to_do_list",
                TextUtils.join(";", ToDoListMaster.getToDoList(getApplicationContext())));

        PutDataRequest request = dataMapRequest.asPutDataRequest();

        Uri uri = new Uri.Builder().scheme(PutDataRequest.WEAR_URI_SCHEME).path("/to_do_path").build();
        Wearable.DataApi.deleteDataItems(mGoogleApiClient, uri);

        Wearable.DataApi.putDataItem(mGoogleApiClient, request);
    }


    private final class ToDoListAdapter extends WearableListView.Adapter {
        private ArrayList<String> mListToDo;

        public ToDoListAdapter(ArrayList<String> listToDo) {
            mListToDo = listToDo;
        }

        public class ItemViewHolder extends WearableListView.ViewHolder {
            private TextView textView;
            public ItemViewHolder(View itemView) {
                super(itemView);
                // find the text view within the custom item's layout
                textView = (TextView) itemView.findViewById(R.id.todo_name);
            }
        }

        @Override
        public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            return new ItemViewHolder(LayoutInflater.from(ToDoListActivity.this).inflate(R.layout.todo_item, null));
        }

        @Override
        public void onBindViewHolder(WearableListView.ViewHolder holder,
                                     int position) {
            ItemViewHolder itemHolder = (ItemViewHolder) holder;
            TextView view = itemHolder.textView;
            view.setText(mListToDo.get(position));
            holder.itemView.setTag(position);
        }

        @Override
        public int getItemCount() {
            return mListToDo.size();
        }
    }

    private void sendMessage() {

        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();

        if (mNode != null && mGoogleApiClient!=null && mGoogleApiClient.isConnected()) {
            Wearable.MessageApi.sendMessage(
                    mGoogleApiClient, mNode.getId(), START_ACTIVITY_PATH, null).setResultCallback(

                    new ResultCallback<MessageApi.SendMessageResult>() {
                        @Override
                        public void onResult(MessageApi.SendMessageResult sendMessageResult) {

                            if (!sendMessageResult.getStatus().isSuccess()) {
                                Log.e(TAG, "Failed to send message with status code: "
                                        + sendMessageResult.getStatus().getStatusCode());
                            }
                        }
                    }
            );
        }else{
            Log.d(TAG, "sendMessage Failed");
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                for (Node node : nodes.getNodes()) {
                    mNode = node;
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "onConnectionSuspended: " + cause);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: " + connectionResult);
    }

}
