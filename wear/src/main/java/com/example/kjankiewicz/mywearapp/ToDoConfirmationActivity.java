package com.example.kjankiewicz.mywearapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.CardScrollView;
import android.support.wearable.view.DelayedConfirmationView;
import android.support.wearable.view.WatchViewStub;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

public class ToDoConfirmationActivity extends Activity implements
        DelayedConfirmationView.DelayedConfirmationListener {

    private DelayedConfirmationView mDelayedView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_confirmation);

        CardScrollView cardScrollView =
                (CardScrollView) findViewById(R.id.card_scroll_view);
        cardScrollView.setCardGravity(Gravity.BOTTOM);

        mDelayedView =
                (DelayedConfirmationView) findViewById(R.id.delayed_confirm);
        mDelayedView.setListener(this);
        mDelayedView.setTotalTimeMs(2000);
        mDelayedView.start();

    }

    @Override
    public void onTimerFinished(View view) {
        Intent intent = new Intent(this, ToDoInfoActivity.class);

        startActivity(intent);
    }

    @Override
    public void onTimerSelected(View view) {
        mDelayedView.reset();
        finish();
    }
}
