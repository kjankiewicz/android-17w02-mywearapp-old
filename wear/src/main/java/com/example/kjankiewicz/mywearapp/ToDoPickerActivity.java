package com.example.kjankiewicz.mywearapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.wearable.view.GridViewPager;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ToDoPickerActivity extends Activity {

    ArrayList<String> mListToDo = new ArrayList<>();

    private List<GroupToDoList> mGroupToDoLists = new ArrayList<GroupToDoList>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);

        mListToDo = ToDoListMaster.getToDoList(getApplicationContext());

        List<String> doItems = new ArrayList<String>();
        List<String> goItems = new ArrayList<String>();
        List<String> otherItems = new ArrayList<String>();
        for (String toDoItem : mListToDo) {
            if (toDoItem.startsWith("do"))
                doItems.add(toDoItem);
            else if (toDoItem.startsWith("go"))
                goItems.add(toDoItem);
            else
                otherItems.add(toDoItem);
        }
        mGroupToDoLists.add(new GroupToDoList("Do",doItems));
        mGroupToDoLists.add(new GroupToDoList("Go",goItems));
        mGroupToDoLists.add(new GroupToDoList("Other",otherItems));

        setupGridViewPager();
    }

    private void setupGridViewPager() {
        final GridViewPager pager = (GridViewPager) findViewById(R.id.pager);
        pager.setAdapter(new ToDoGridPagerAdapter(this, mGroupToDoLists, getFragmentManager()));
    }

    public static class GroupToDoList {
        private List<String> mToDos;
        private String mGroupName;

        public GroupToDoList(String groupName, List<String> toDos) {
            mGroupName = groupName;
            mToDos = toDos;
        }

        public String getGroupName(int page) {
            if (page == 0) {
                return mGroupName;
            } else {
                return null;
            }
        }

        public String getText(int page) {
            if (page == 0) {
                return null;
            } else {
                return mToDos.get(page - 1);
            }
        }

        public int getPageCount() {
            return mToDos.size() + 1;
        }
    }

}
