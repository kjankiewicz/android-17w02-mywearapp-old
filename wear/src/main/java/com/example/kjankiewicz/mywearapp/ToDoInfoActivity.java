package com.example.kjankiewicz.mywearapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.CardScrollView;
import android.view.Gravity;
import android.widget.TextView;

/**
 * Created by kjankiewicz on 2015-05-31.
 */
public class ToDoInfoActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_info);

        CardScrollView cardScrollView =
                (CardScrollView) findViewById(R.id.card_scroll_view);
        cardScrollView.setCardGravity(Gravity.BOTTOM);

        Intent conf_intent = new Intent(this, ConfirmationActivity.class);
        conf_intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                ConfirmationActivity.SUCCESS_ANIMATION);
        conf_intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
                getString(R.string.confirmation_message));
        startActivity(conf_intent);
    }
}
